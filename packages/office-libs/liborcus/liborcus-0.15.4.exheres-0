# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist="2" with_opt=true ]

SUMMARY="Standalone file import filter library for spreadsheet documents"
DESCRIPTION="
Includes filters for xlsx, ods, csv, and generic XML formats.
"
HOMEPAGE="https://gitlab.com/orcus/orcus#tab-readme"
DOWNLOADS="https://kohei.us/files/orcus/src/${PNV}.tar.xz"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/mdds:1.5[>=1.4.99]
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/boost[>=1.36.0]
        dev-libs/libixion[>=0.15.0]
        sys-libs/zlib
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-spreadsheet-model
    --disable-debug-utils
    --disable-static
    --disable-werror
    --with-cpu-features
    --with-gnumeric-filter
    --with-ods-filter
    --with-tools
    --with-xls-xml-filter
    --with-xlsx-filter
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( python )

