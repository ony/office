# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require python [ blacklist="2" with_opt=true ]

SUMMARY="General purpose formula parser & interpreter that can calculate multiple 'cells'"
DESCRIPTION="
A library for calculating the results of formula expressions stored in multiple named targets, or
“cells”. The cells can be referenced from each other, and the library takes care of resolving their
dependencies automatically upon calculation. The caller can run the calculation routine either in a
single-threaded mode, or a multi-threaded mode. The library also supports re-calculations where the
contents of one or more cells have been modified since the last calculation, and a partial
calculation of only the affected cells need to be calculated.
"
HOMEPAGE="https://gitlab.com/ixion/ixion#tab-readme"
DOWNLOADS="https://kohei.us/files/ixion/src/${PNV}.tar.xz"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# libtool picking up the old version
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/mdds:1.4[>=1.4.0]
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/boost[>=1.36]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-threads
    --disable-cuda
    --disable-debug
    --disable-static
    --disable-static-boost
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( python )

src_prepare() {
    # honor flags
    edo sed \
        -e 's/ -O2//' \
        -i configure.ac

    autotools_src_prepare
}

